# Telegram to MySql

#### Saves new messages from Telegram's group/channel to MySql database

*db_data.csv* file should be filled properly, like: <br>

&nbsp;&nbsp;&nbsp;&nbsp;*db_name:name_of_db* <br>
&nbsp;&nbsp;&nbsp;&nbsp;*user:John_Doe* <br>
&nbsp;&nbsp;&nbsp;&nbsp;*password:pass123* <br>
&nbsp;&nbsp;&nbsp;&nbsp;*host:localhost* <br>

In the same way should be filled *tg_login_data.csv*'s following fields: <br>

&nbsp;&nbsp;&nbsp;&nbsp;*code* <br>
&nbsp;&nbsp;&nbsp;&nbsp;*api_id* <br>
&nbsp;&nbsp;&nbsp;&nbsp;*api_hash* <br>
&nbsp;&nbsp;&nbsp;&nbsp;*phone_number* <br>


#### Alembic migrations

* `alembic init alembic`
* update *sqlalchemy.url* line in *alembic.ini* <br>
e.g. `sqlalchemy.url = mysql://user:password@localhost/db_name`
* make shure that *db_data.csv* is updated in case of password have been changed
* replace *alembic/env.py* file with repo's *env.py*
* `alembic revision --autogenerate -m "initial"`
* `alembic upgrade head` 

To show migration use `alembic history --verbose`


Start `run.py`
