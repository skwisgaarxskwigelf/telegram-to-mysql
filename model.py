from sqlalchemy import Column, String, Text, BigInteger, Float, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy import create_engine
import db_engine

Base = declarative_base()


class Message(Base):
    """
    Create a messages' table
    """
    __tablename__ = 'messages'

    id = Column(BigInteger, primary_key=True)
    tg_id = Column(BigInteger, nullable=False)
    grouped_id = Column(BigInteger, default=0)
    sender_id = Column(BigInteger, ForeignKey('senders.id'))
    message = Column(Text, default='')
    timestamp = Column(Float)
    document_id = Column(BigInteger, default=0)
    group_id = Column(BigInteger, ForeignKey('groups.id'), default=0)
    channel_id = Column(BigInteger, ForeignKey('channels.id'), default=0)
    sender = relationship("Sender")
    group = relationship("Group")
    channel = relationship("Channel")

    def __init__(self, tg_id, grouped_id, sender_id, message, message_timestamp, document_id, group_id, channel_id):
        self.tg_id = tg_id
        self.grouped_id = grouped_id
        self.sender_id = sender_id
        self.message = message
        self.message_timestamp = message_timestamp
        self.document_id = document_id
        self.group_id = group_id
        self.channel_id = channel_id

    def __repr__(self):
        return '<Telegram id is {}>'.format(self.tg_id)


class Group(Base):
    """
    Create a groups' table
    """
    __tablename__ = 'groups'

    id = Column(BigInteger, primary_key=True)
    tg_id = Column(BigInteger, unique=True, nullable=False)
    group_link = Column(String(30))

    def __init__(self, tg_id, group_link):
        self.tg_id = tg_id
        self.group_link = group_link

    def __repr__(self):
        return '<Telegram id is {0}, group link is {1}>'.format(self.tg_id, self.channel_link)


class Channel(Base):
    """
    Create a channels' table
    """
    __tablename__ = 'channels'

    id = Column(BigInteger, primary_key=True)
    tg_id = Column(BigInteger, unique=True, nullable=False)
    channel_link = Column(String(30))

    def __init__(self, tg_id, channel_link):
        self.tg_id = tg_id
        self.channel_link = channel_link

    def __repr__(self):
        return '<Telegram id is {0}, channel link is {1}>'.format(self.tg_id, self.channel_link)


class Sender(Base):
    """
    Create a senders' table
    """
    __tablename__ = 'senders'

    id = Column(BigInteger, primary_key=True)
    tg_sender_id = Column(BigInteger, unique=True, nullable=False)
    username = Column(String(30))

    def __init__(self, tg_sender_id, username):
        self.tg_sender_id = tg_sender_id
        self.username = username

    def __repr__(self):
        return '<Telegram sender_id is {}>'.format(self.tg_sender_id)


class Document(Base):
    """
    Create an documents' table
    """
    __tablename__ = 'documents'

    id = Column(BigInteger, primary_key=True)
    tg_id = Column(BigInteger, nullable=False)
    grouped_id = Column(BigInteger, default=0)

    def __init__(self, tg_id, grouped_id, message_id):
        self.tg_id = tg_id
        self.grouped_id = grouped_id
        self.message_id = message_id

    def __repr__(self):
        return '<Message id {}>'.format(self.message_id)


engine = create_engine(db_engine.get_engine_address())

session = sessionmaker()
session.configure(bind=engine)
Base.metadata.create_all(engine)
