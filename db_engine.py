import pandas as pd

df_list = pd.read_csv('db_data.csv', delimiter=':', header=None, encoding='utf-8').iloc[:, 1].values.tolist()


def get_engine_address():
    return "mysql://" + df_list[1].strip() + ":" + df_list[2].strip() + "@" + df_list[3].strip() + "/" + df_list[0].strip() + "?charset=utf8mb4" 
