class CheckFields:
    """
    Checking data types
    Before saving to database or server
    """
    @staticmethod
    def check_post(message: object):
        if hasattr(message, 'message'):
            return message.message if type(message.message) is not None else ''
        return ''

    @staticmethod
    def check_document(message: object)-> bool:
        """
        :param message:
        :return:
        """
        if hasattr(message, 'media'):
            if hasattr(message.media, 'document'):
                if type(message.media.document) is not None:
                    return True
        return False


    @staticmethod
    def check_grouped_id(message: object):
        if hasattr(message, 'grouped_id'):
            return message.grouped_id if message.grouped_id is not None else 0
        return 0