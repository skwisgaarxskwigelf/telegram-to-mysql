import pandas as pd
import asyncio
from telethon import TelegramClient, sync, events, utils
import save_message as sm
import logging
import os

logging.basicConfig(filename="logs/tg_extract.log", level=logging.WARNING)

# creating a list of following structure:
# [code, api_id, api_hash, phone_number]
tg_login_list = pd.read_csv('tg_login_data.csv', delimiter=':', header=None,
                            encoding='utf-8').iloc[:, 1].values.tolist()


async def save_media_on_server(message):
    doc_path = "docs/" + message.media.document.attributes[0].file_name.split('.')[0]

    # saving to server
    await client.download_media(message, doc_path)

    # output result to console
    if os.geteuid:
        print("Document tg_id {0} is saved, channel tg_id {1}, message_tg_id {2}, grouped_id {3}\n".format(
            message.media.document.id, message.chat.id, message.id, message.grouped_id))

    # log result
    with open("logs/saved_documents.log", "a+") as saved_documents:
        try:
            saved_documents.write(
                "Document tg_id {0} is saved, channel tg_id {1}, message_tg_id {2}, grouped_id {3}\n".format(
                    message.media.document.id, message.chat.id, message.id, message.grouped_id))
        except UnicodeEncodeError as e:
            pass


# creating client for connection
client = TelegramClient(tg_login_list[3].strip() + ".session", tg_login_list[1].strip(), tg_login_list[2].strip())


# creating event to receive new messages
@client.on(events.NewMessage)
async def event_handler(event):
    group_pk = channel_pk = None

    if hasattr(event.sender, 'is_self'):
        if not event.sender.is_self:
            group_pk = sm.get_group_pk(event.chat)
    else:
        channel_pk = sm.get_channel_pk(event.chat)

    sender_pk = sm.get_sender_pk(event.sender)

    document_pk = None
    # checking if Telegram message is document
    if sm.check_document(event.message):
        # checking if document has needed format
        if sm.check_doc_format(event.message.media.document.attributes[0].file_name.split('.')[1]):
            # save to db
            document_pk = sm.get_document_pk(event.message)

    if sm.save_message(event.message, sender_pk, group_pk, channel_pk, document_pk):
        # output result to console
        if os.geteuid:
            print("Message {0} from channel {1} is saved".format(event.message.id, event.chat.id))
        # log result
        with open("logs/saved_messages.log", "a+") as saved_messages:
            try:
                saved_messages.write("Message {0} from channel {1} is saved\n".format(event.message.id, event.chat.id))
            except UnicodeEncodeError as e:
                pass
        # save document on the server if needed
        if document_pk is not None:
            await save_media_on_server(event.message)


if __name__ == '__main__':
    client.start()
    client.run_until_disconnected()
