from sqlalchemy import create_engine, exc
from sqlalchemy.sql import insert, select
import db_engine
import db_metadata
from MySQLdb import _exceptions
from datetime import datetime


class MysqlImport:
    def __init__(self):
        self.engine_address = db_engine.get_engine_address()
        self.messages = db_metadata.get_meta_messages()
        self.groups = db_metadata.get_meta_groups()
        self.channels = db_metadata.get_meta_channels()
        self.senders = db_metadata.get_meta_senders()
        self.documents = db_metadata.get_meta_documents()

    def create_engine(self):
        return create_engine(self.engine_address)

    def get_conn(self):
        try:
            return self.create_engine().connect()
        except (exc.OperationalError, _exceptions.OperationalError) as e:
            print("Error: {}".format(e))

    def get_group_id_pkey(self, tg_id: int):
        """

        :param tg_id:
        :return:
        """
        res = self.get_conn().execute(select([self.groups]).where(self.groups.c.tg_id == tg_id)).fetchone()
        if res is not None:
            return res[0]
        return res

    def get_channel_id_pkey(self, tg_id: int):
        """

        :param tg_id:
        :return:
        """
        res = self.get_conn().execute(select([self.channels]).where(self.channels.c.tg_id == tg_id)).fetchone()
        if res is not None:
            return res[0]
        return res

    def get_sender_id_pkey(self, tg_sender_id: int):
        """

        :param tg_sender_id:
        :return:
        """
        res = self.get_conn().execute(
            select([self.senders]).where(self.senders.c.tg_sender_id == tg_sender_id)).fetchone()
        if res is not None:
            return res[0]
        return res

    def get_groups_ids(self) -> set:
        """

        :return:
        """
        groups_ids = self.get_conn().execute("select tg_id from groups").fetchall()
        res = set()
        if len(groups_ids):
            for _ in groups_ids:
                res.add(_[0])
        return res

    def get_channels_ids(self) -> set:
        """

        :return:
        """
        channels_ids = self.get_conn().execute("select tg_id from channels").fetchall()
        res = set()
        if len(channels_ids):
            for _ in channels_ids:
                res.add(_[0])
        return res

    def get_senders_ids(self) -> set:
        """

        :return:
        """
        senders_ids = self.get_conn().execute("select tg_sender_id from senders").fetchall()
        res = set()
        if len(senders_ids):
            for _ in senders_ids:
                res.add(_[0])
        return res

    def save_group(self, tg_id: int, group_link: str = ''):
        """

        :param tg_id:
        :param group_link:
        :return:
        """
        try:
            ins = self.get_conn().execute(self.groups.insert().values(tg_id=tg_id, group_link=group_link))
            if ins.is_insert:
                return ins.inserted_primary_key[0]
        except (exc.IntegrityError, _exceptions.IntegrityError) as e:
            with open('logs/db/insert.log', 'a+', encoding='utf8') as ins_error:
                try:
                    ins_error.write('save_group error: {0}, {1}'.format(e, datetime.now()))
                except UnicodeEncodeError:
                    pass
            return None

    def save_channel(self, tg_id: int, channel_link: str = ''):
        """

        :param tg_id:
        :param channel_link:
        :return:
        """
        try:
            ins = self.get_conn().execute(self.channels.insert().values(tg_id=tg_id, channel_link=channel_link))
            if ins.is_insert:
                return ins.inserted_primary_key[0]
        except (exc.IntegrityError, _exceptions.IntegrityError) as e:
            with open('logs/db/insert.log', 'a+', encoding='utf8') as ins_error:
                try:
                    ins_error.write('save_channel error: {0}, {1}'.format(e, datetime.now()))
                except UnicodeEncodeError:
                    pass
            return None

    def save_sender(self, tg_sender_id: int, username: str = ''):
        """

        :param tg_sender_id:
        :param username:
        :return:
        """
        try:
            ins = self.get_conn().execute(self.senders.insert().values(tg_sender_id=tg_sender_id, username=username))
            if ins.is_insert:
                return ins.inserted_primary_key[0]
        except (exc.IntegrityError, _exceptions.IntegrityError) as e:
            with open('logs/db/insert.log', 'a+', encoding='utf8') as ins_error:
                try:
                    ins_error.write('save_sender error: {0}, {1}'.format(e, datetime.now()))
                except UnicodeEncodeError:
                    pass
            return None

    def save_document(self, tg_id: int, grouped_id: int = 0):
        """

        :param tg_id:
        :param grouped_id:
        :return:
        """
        try:
            ins = self.get_conn().execute(self.documents.insert().values(tg_id=tg_id, grouped_id=grouped_id))
            if ins.is_insert:
                return ins.inserted_primary_key[0]
        except (exc.IntegrityError, _exceptions.IntegrityError) as e:
            with open('logs/db/insert.log', 'a+', encoding='utf8') as ins_error:
                try:
                    ins_error.write('save_documents error: {0}, {1}'.format(e, datetime.now()))
                except UnicodeEncodeError:
                    pass
            return None

    def save_message(self, tg_id: int, sender_id: int, timestamp: int, message: str = '', grouped_id: int = 0,
                     group_id: int = 0, channel_id: int = 0, document_id: int = 0) -> bool:
        """

        :param tg_id:
        :param sender_id:
        :param timestamp:
        :param message:
        :param grouped_id:
        :param group_id:
        :param channel_id:
        :param document_id:
        :return:
        """
        try:
            return self.get_conn().execute(
                self.messages.insert().values(tg_id=tg_id, sender_id=sender_id, timestamp=timestamp, message=message,
                                              grouped_id=grouped_id, group_id=group_id, channel_id=channel_id,
                                              document_id=document_id)).is_insert
        except (exc.IntegrityError, _exceptions.IntegrityError) as e:
            with open('logs/db/insert.log', 'a+', encoding='utf8') as ins_error:
                try:
                    ins_error.write('save_message error: {0}, {1}'.format(e, datetime.now()))
                except UnicodeEncodeError:
                    pass
            return False
