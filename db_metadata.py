from sqlalchemy import MetaData, Table, Column, String, Text, BigInteger, Float, ForeignKey

metadata = MetaData()

"""
Defining tables
"""


def get_meta_messages():
    return Table('messages', metadata,
                 Column('id', BigInteger, primary_key=True),
                 Column('tg_id', BigInteger, nullable=False),
                 Column('grouped_id', BigInteger, default=0),
                 Column('sender_id', BigInteger, ForeignKey('senders.id')),
                 Column('message', Text, default=''),
                 Column('timestamp', Float),
                 Column('document_id', BigInteger, default=0),
                 Column('group_id', BigInteger, ForeignKey('groups.id'), default=0),
                 Column('channel_id', BigInteger, ForeignKey('channels.id'), default=0)
                 )


def get_meta_groups():
    return Table('groups', metadata,
                 Column('id', BigInteger, primary_key=True),
                 Column('tg_id', BigInteger, unique=True, nullable=False),
                 Column('group_link', String(30))
                 )


def get_meta_channels():
    return Table('channels', metadata,
                 Column('id', BigInteger, primary_key=True),
                 Column('tg_id', BigInteger, unique=True, nullable=False),
                 Column('channel_link', String(30))
                 )


def get_meta_senders():
    return Table('senders', metadata,
                 Column('id', BigInteger, primary_key=True),
                 Column('tg_sender_id', BigInteger, unique=True, nullable=False),
                 Column('username', String(30))
                 )


def get_meta_documents():
    return Table('documents', metadata,
                 Column('id', BigInteger, primary_key=True),
                 Column('tg_id', BigInteger, nullable=False),
                 Column('grouped_id', BigInteger, default=0)
                 )
