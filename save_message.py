from mysql_import import MysqlImport
from check_fields import CheckFields
from telethon.tl.types import Channel
from telethon.tl.patched import Message

mysql_import = MysqlImport()


# get primary key of groups's tg_id if tg_id exists
def db_get_group_id_pkey(tg_id: int):
    return mysql_import.get_group_id_pkey(tg_id)


# get primary key of channel's tg_id if tg_id exists
def db_get_channel_id_pkey(tg_id: int):
    return mysql_import.get_channel_id_pkey(tg_id)


# get primary key of senders's tg_id if tg_id exists
def db_get_sender_id_pkey(tg_sender_id: int):
    return mysql_import.get_sender_id_pkey(tg_sender_id)


def db_save_group(tg_id: int, group_link: str = ''):
    return mysql_import.save_group(tg_id, group_link)


def db_save_channel(tg_id: int, channel_link: str = ''):
    return mysql_import.save_channel(tg_id, channel_link)


def db_save_sender(tg_sender_id: int, username: str = ''):
    return mysql_import.save_sender(tg_sender_id, username)


def db_save_document(tg_id: int, grouped_id: int = 0):
    return mysql_import.save_document(tg_id, grouped_id)


# try to get db's primary key of group
def get_group_pk(chat: Channel) -> int:
    group_pk = db_get_group_id_pkey(int(chat.id))
    chat_username = None
    if group_pk is None:
        try: 
            chat_username = chat.username
        except AttributeError:
            pass
        group_pk = db_save_group(int(chat.id), chat_username)
    return group_pk


# try to get db's primary key of channel
def get_channel_pk(chat: Channel) -> int:
    channel_pk = db_get_channel_id_pkey(int(chat.id))
    if channel_pk is None:
        channel_pk = db_save_channel(int(chat.id), chat.username)
    return channel_pk


# try to get db's primary key of sender
def get_sender_pk(sender) -> int:
    sender_pk = db_get_sender_id_pkey(int(sender.id))
    if sender_pk is None:
        sender_pk = db_save_sender(int(sender.id), sender.username)
    return sender_pk


# checking if Telegram message is document
def check_document(message: Message):
    return CheckFields.check_document(message)


def check_doc_format(doc_format: str = ''):
    formats = ['pdf', 'doc', 'docx']
    if doc_format in formats:
        return True
    return False


# save document and returns document's primary key
def get_document_pk(message: Message) -> int:
    return db_save_document(message.media.document.id, message.grouped_id)


def save_message(message: object, sender_pk: int, group_pk: int = 0, channel_pk: int = 0, document_pk: int = 0) -> bool:
    """
    saving the post
    """
    return mysql_import.save_message(message.id, sender_pk, message.date.timestamp(), CheckFields.check_post(message),
                                     CheckFields.check_grouped_id(message), group_pk, channel_pk, document_pk)
